# Check for new messages

Request to check for new updates

**URL** : `/api/status/?keyword={keyword}&limit={limit}&time={time}&option={Option}`

**Method** : `GET`

**Note** : There's also 'best' keyword that gets the most orders accross all keywords.

- There are 3 options : ali, ebay , amazon
- every option show different products. you need to have 3 tabs to select on which one to do. 
- only show options that are available in that keyword when it was first submitted. 


**TIME PARAMETER** : add time option and have three tabs : today/week/all


**Url example:**

```
    GET /api/status/?keyword=hoodie&limit=40&time=today&option=ali
```


## Success Response

**Condition** : Keeps track of what it does/how many new orders and other features. 

**IMPORTANT Note**: Check for it every X seconds.

**Code** : `200 OK`

**Content example**

```json
    [
    {
        "Name": "chu mark 2018 Winter Jacket Womens Coats Short Female",
        "Id": "32686337093",
        "ProductUrl": "https://www.aliexpress.com/item/chu-mark-2018-Winter-Jacket-Womens-Coats-Short-Female/32686337093.html",
        "ImageUrl": "//ae01.alicdn.com/kf/HTB1xRlbRXXXXXXDXXXXq6xXFXXX0/2018-Winter-font-b-Jacket-b-font-women-Plus-Size-Womens-Parkas-Thicken-Outerwear-solid-hooded.jpg",
        "Stars": "4.8",
        "ActionUrl": "//m.aliexpress.com/item/32686337093.html?pid=808_0000_0109",
        "BigSale": false,
        "NewOrders": -533,
        "Orders": 4686,
        "BaseOrder": 5219,
        "Changed": false,
        "FreeShipping": true,
        "HasEpackets": true,
        "Price": 18.92
    },
    {
        "Name": "GMANCL Hip Hop hooded zipper Sweatshirt Slim Fit Men Hoody",
        "Id": "32793404345",
        "ProductUrl": "https://www.aliexpress.com/item/GMANCL-Hip-Hop-hooded-zipper-Sweatshirt-Slim-Fit-Men-Hoody/32793404345.html",
        "ImageUrl": "//ae01.alicdn.com/kf/HTB1078iSVXXXXX.XFXXq6xXFXXX1/2017-Fashion-Hoodies-Men-Sudaderas-Hombre-Hip-Hop-Mens-Brand-Solid-hooded-zipper-font-b-Hoodie.jpg",
        "Stars": "4.5",
        "ActionUrl": "//m.aliexpress.com/item/32793404345.html?pid=808_0000_0109",
        "BigSale": false,
        "NewOrders": -425,
        "Orders": 2638,
        "BaseOrder": 3063,
        "Changed": false,
        "FreeShipping": true,
        "HasEpackets": true,
        "Price": 12.25
    },
    ...
```

## Error Responses

**Condition** : If keyword not found

**Code** : `404 NotFound`


**Content** : `[]`
