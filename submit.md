# Submit new Number 


**URL** : `/api/submit`

**Method** : `POST`

**Data constraints**

Keyword is required and others are optional.

**Data example:** All fields must be sent.

```json
{
    "Keyword": "example1",
    "LimitProducts": 100,
    "CheckEpacketIn": "US",
    "CheckEveryXHour": 24,
    "Filters": {
        "ShipToCountry": "US",
        "MinPrice": "",
        "MaxPrice": "",
        "FreeShipping": "true" ,
        "MaxBaseProducts": 1000
    },
    "Amazon": true,
    "Ebay": false,
    "AliExpress": true
}
```


## Success Response

**Condition** : If it success, it will send a success response

**Code** : `200 OK`

**Content example**

```
Will Start Parsing soon
```

## Error Responses

**Condition** : If there is some issue

**Code** : `401 Unauthorized`
