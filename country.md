# Find the countries that ordered the most. 

Find countries with most ordered units. and Keeps track of how many new orders for each country.

**URL** : `/api/country/?productid={productid}`

**Method** : `GET`

**Url example:**

```
    GET /api/country/?productid=32825230750
```


## Success Response

**IMPORTANT Note**: It takes 1-5 minutes to show up.

**Code** : `200 OK`

**Content example**

```json
    [
    {
        "Key": "CL",
        "Value": 58
    },
    {
        "Key": "BR",
        "Value": 33
    },
    {
        "Key": "AR",
        "Value": 28
    },
    {
        "Key": "CO",
        "Value": 13
    },
    {
        "Key": "RU",
        "Value": 7
    },
    {
        "Key": "AU",
        "Value": 6
    },
    {
        "Key": "MX",
        "Value": 5
    },
    {
        "Key": "US",
        "Value": 5
    },
    {
        "Key": "UY",
        "Value": 4
    },
    {
        "Key": "MA",
        "Value": 2
    },
    {
        "Key": "UA",
        "Value": 2
    },
    {
        "Key": "BY",
        "Value": 1
    },
    {
        "Key": "LV",
        "Value": 1
    },
    {
        "Key": "KZ",
        "Value": 1
    },
    {
        "Key": "NZ",
        "Value": 1
    },
    {
        "Key": "DE",
        "Value": 1
    },
    {
        "Key": "PE",
        "Value": 1
    },
    {
        "Key": "EC",
        "Value": 1
    },
    {
        "Key": "PK",
        "Value": 1
    },
]
```

## Awaiting responses

**Condition** : If country request was just submit, it takes a bit of time to get the details, thus it needs to refresh every few seconds until you get the final request 

**Content** : `Requesting country is Sent`

**Condition** : If country request was still unsubmit. say that it's still waiting

**Content** : `Still waiting ...`


## Error Responses

**Condition** : If keyword not found

**Code** : `404 NotFound`


**Content** : `[]`
