# RESTAPIDocs

These examples need to be revised carefully as it shows how to work with the REST API


## Login requests.

* [submit keyword](submit.md) : `POST /api/submit`


## Check for updates

* [get updates of keyword](updates.md) : `GET /api/status/?keyword={keyword}&limit=10`

## Get countries of a product id

* [get countries of product](country.md) : `/api/country/?productid={productid}`

## Live URL

here's a demo link for you to test with : http://185.180.223.91:8182/api/